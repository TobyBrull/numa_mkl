#ifndef INCLUDE_GUARD_NUMA_MKL_DOT_H_
#define INCLUDE_GUARD_NUMA_MKL_DOT_H_

#include "numa/misc/benchmark.hpp"
#include "numa/misc/candidates.hpp"

#include "numa/lina/dot.hpp"

#include "mkl.h"

namespace numa::mkl
{
  /*
   * retval = alpha * A * B + beta * retval
   */
  template<typename T>
  void dot (matrix_span<T> const& retval, T alpha, T beta,
            matrix_span<T const> const& A,
            matrix_span<T const> const& B);

  class candidate_dot
  {
    public:
      std::string name () const;

      misc::matrix_supported_type
      operator () (misc::matrix_supported_type const& A_st,
                   misc::matrix_supported_type const& B_st,
                   misc::supported_type,
                   index_t num_threads,
                   cpu_target) const;
  };
}

#include "dot.inl"

#endif
