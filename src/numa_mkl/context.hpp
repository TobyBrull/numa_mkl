#ifndef INCLUDE_GUARD_NUMA_MKL_CONTEXT_H_
#define INCLUDE_GUARD_NUMA_MKL_CONTEXT_H_

#include "numa/lina/cpu_target.hpp"

#include "numa/boot/error.hpp"

#include "mkl.h"

namespace numa::mkl
{
  int cpu_target_to_isa (cpu_target);
}

#include "context.inl"

#endif
