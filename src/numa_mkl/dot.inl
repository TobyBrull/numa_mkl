namespace numa::mkl
{
  template<typename T>
  void
  dot (
      matrix_span<T> const& retval,
      T alpha,
      T beta,
      matrix_span<T const> const& A,
      matrix_span<T const> const& B)
  {
    THROW_IF_NOT (
        (retval.stride_ [0] == 1) &&
        (A.stride_ [0] == 1) &&
        (B.stride_ [0] == 1),
            "numa::mkl::dot: all matrices must be column-major");

    index_t const n = A.size_ [0];
    index_t const m = A.size_ [1];
    index_t const p = B.size_ [1];

    index_t const ld_rv = retval.stride_ [1];
    index_t const ld_A = A.stride_ [1];
    index_t const ld_B = B.stride_ [1];

    THROW_IF_NOT (
        (n == retval.size_ [0]) &&
        (m == B.size_ [0]) &&
        (p == retval.size_ [1]),
            "numa::mkl::dot: matrix sizes not matching");

    if constexpr (c_type<T> == c_type<double>)
    {
      cblas_dgemm (
          CblasColMajor, CblasNoTrans, CblasNoTrans,
          n, p, m,
          alpha, A.origin_, ld_A, B.origin_, ld_B,
          beta, retval.origin_, ld_rv);
    }
    else if constexpr (c_type<T> == c_type<float>)
    {
      cblas_sgemm (
          CblasColMajor, CblasNoTrans, CblasNoTrans,
          n, p, m,
          alpha, A.origin_, ld_A, B.origin_, ld_B,
          beta, retval.origin_, ld_rv);
    }
    else
    {
      dump_type<T> _;
    }
  }
}
