#include "dot.hpp"

#include "context.hpp"

#include "numa/ctxt/env_var.hpp"

#include "numa/misc/benchmark_context.hpp"
#include "numa/cell/serialize_binary.hpp"

#include "pstreams/pstream.h"

namespace numa::mkl
{
  std::string
  candidate_dot::
  name () const
  {
    return "mkl";
  }

  misc::matrix_supported_type
  candidate_dot::
  operator () (
      misc::matrix_supported_type const& A_st,
      misc::matrix_supported_type const& B_st,
      misc::supported_type const st,
      index_t const num_threads,
      cpu_target const ct) const
  {
    /*
     * The following requires running in a separate process because MKL doesn't
     * allow for the optimization path to be changed between calls to MKL
     * functions.
     */
    std::string const command =
        env_var_default ("command", "bin/tool.dot phase_level=off");

    redi::pstream ps (command);

    auto& os = ps;
    auto& is = ps;

    os << st << '\n';
    os << num_threads << '\n';
    os << ct << '\n';
    os << 1.0 << '\n';
    os << 0.0 << '\n';

    auto const printer = [&os] (auto const& x) { to_binary (os, x); };
    std::visit (printer, A_st);
    std::visit (printer, B_st);

    os << redi::peof;

    misc::benchmark_duration bd;
    is >> bd;

    misc::benchmark_context::notify (bd);

    {
      char newline;
      is.read (&newline, 1);
      THROW_IF_NOT (newline == '\n', "Invalid format");
    }

    THROW_IF_NOT(A_st.index() == B_st.index());
    misc::matrix_supported_type retval;
    std::visit([&]<typename T> (matrix<T> const&) {
        retval = make_from_binary<matrix<T>> (is);
        }, A_st);
    return retval;
  }
}
