namespace numa::mkl
{
  inline
  int
  cpu_target_to_isa (
      cpu_target const ct)
  {
    switch (ct)
    {
      case cpu_target::generic:   return MKL_ENABLE_SSE4_2;
      case cpu_target::sse4_2:    return MKL_ENABLE_SSE4_2;
      case cpu_target::avx:       return MKL_ENABLE_AVX;
      case cpu_target::avx2:      return MKL_ENABLE_AVX2;
      case cpu_target::avx512:    return MKL_ENABLE_AVX512;
    }

    UNREACHABLE ();
  }
}
