#ARG BASE_IMAGE=silkeh/clang:10
ARG BASE_IMAGE=gcc:10.2

FROM $BASE_IMAGE as dev

RUN apt-get update \
  && apt-get install -y git vim \
      gdb lldb clang-tidy clang-format cppcheck ccache \
      python3 python3-pip python3-setuptools

RUN pip3 install conan==1.29.0 cmake==3.18.2 ninja==1.10.0 \
  && conan profile new default --detect \
  && conan profile update settings.compiler.libcxx=libstdc++11 default

# Install MKL
RUN apt-get install -y software-properties-common \
  && apt-add-repository non-free \
  && apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends intel-mkl-full

# Install Numa
RUN mkdir -p /numa \
  && wget -q -c https://gitlab.com/TobyBrull/numa/-/jobs/artifacts/master/download?job=all -O /numa/all.zip \
  && cd /numa \
  && unzip all.zip \
  && mv output/*.deb . \
  && rm -rf output/ all.zip \
  && dpkg -i numa-Release.deb

# Install PStreams
RUN apt-get install -y libpstreams-dev

FROM dev as build
COPY . /app

# Via Debian packages.
RUN mkdir -p /build/via_debian \
  && cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DUSE_CONAN=OFF -S /app -B /build/via_debian \
  && cmake --build /build/via_debian \
  && cd /build/via_debian \
  && ctest

# Via conan.
ARG CI_JOB_TOKEN
RUN conan remote add gitlab https://gitlab.com/api/v4/packages/conan \
  && conan user ci_user -p ${CI_JOB_TOKEN} --remote=gitlab
RUN mkdir -p /build/via_conan \
  && cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DUSE_CONAN=ON -S /app -B /build/via_conan \
  && cmake --build /build/via_conan \
  && cd /build/via_conan \
  && ctest
