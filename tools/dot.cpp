#include "numa_mkl/context.hpp"
#include "numa_mkl/dot.hpp"

#include "numa/cell/serialize_json.hpp"
#include "numa/cell/serialize_binary.hpp"

#include "numa/misc/main.hpp"

using namespace numa;

namespace
{
  template<typename T>
  void
  main_impl ()
  {
    auto const alpha = make_from_pretty<T> (std::cin);
    auto const beta = make_from_pretty<T> (std::cin);

    {
      char newline;
      std::cin.read (&newline, 1);
      THROW_IF_NOT (newline == '\n', "Invalid format");
    }

    auto const A = make_from_binary<matrix<T>> (std::cin);
    auto const B = make_from_binary<matrix<T>> (std::cin);

    auto const A_span = A.span_const ();
    auto const B_span = B.span_const ();

    auto retval = make_cell<matrix<T>> (A.size () [0], B.size () [1]) ();
    auto const retval_span = retval.span ();

    misc::benchmark_context bc;

    {
      auto t = misc::benchmark_context::timer ();
      mkl::dot<T> (retval_span, alpha, beta, A_span, B_span);
    }

    std::cout << bc.duration () << '\n';

    to_binary (std::cout, retval);
  }
}

int main (int argc, char const** argv)
{
  return numa::misc::main (argc, argv,
    []
    {
      auto const st = make_from_pretty<misc::supported_type> (std::cin);
      auto const num_threads = make_from_pretty<index_t> (std::cin);
      auto const ct = make_from_pretty<cpu_target> (std::cin);

      mkl_enable_instructions (mkl::cpu_target_to_isa (ct));
      mkl_set_num_threads (num_threads);

      switch (st)
      {
        case misc::supported_type::float_t:
        {
          return main_impl<float> ();
        }
        break;

        case misc::supported_type::double_t:
        {
          return main_impl<double> ();
        }
        break;
      }

      UNREACHABLE ();
    });
}
