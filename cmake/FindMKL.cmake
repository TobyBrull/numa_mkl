find_path(MKL_INCLUDE_DIR NAMES mkl.h HINTS ${MKL_DIR})
if (NOT MKL_INCLUDE_DIR)
  find_path(MKL_INCLUDE_DIR NAMES mkl/mkl.h)
  set(MKL_INCLUDE_DIR "${MKL_INCLUDE_DIR}/mkl")
endif ()

find_library(MKL_LIB_RT NAMES mkl_rt HINTS ${MKL_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MKL DEFAULT_MSG MKL_INCLUDE_DIR MKL_LIB_RT)

add_library(PKG::intel_mkl SHARED IMPORTED)
set_target_properties(PKG::intel_mkl PROPERTIES IMPORTED_LOCATION ${MKL_LIB_RT})
target_include_directories(PKG::intel_mkl INTERFACE ${MKL_INCLUDE_DIR})
