if (NOT DEFAULT_CMAKE_BUILD_TYPE)
  message (FATAL_ERROR
    "DEFAULT_CMAKE_BUILD_TYPE needs to be set for 'cmake/default_build_type.cmake'")
else ()
  if (NOT CMAKE_BUILD_TYPE)
    set (CMAKE_BUILD_TYPE "${DEFAULT_CMAKE_BUILD_TYPE}" CACHE STRING "" FORCE)
  endif ()
  message ("Build type set to '${CMAKE_BUILD_TYPE}'.")
endif ()
