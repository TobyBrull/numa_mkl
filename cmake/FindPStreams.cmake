find_path(PSTREAMS_INCLUDE_DIR NAMES pstreams/pstream.h)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PStreams DEFAULT_MSG PSTREAMS_INCLUDE_DIR)

add_library(PKG::pstreams INTERFACE IMPORTED)
target_include_directories(PKG::pstreams INTERFACE ${PSTREAMS_INCLUDE_DIR})
