#include "numa_mkl/context.hpp"
#include "numa_mkl/dot.hpp"

#include "numa/misc/main.hpp"

int main (int argc, char const** argv)
{
  return numa::misc::main (argc, argv,
    []
    {
      using namespace numa;
      using namespace numa::misc;

      auto setup = [&]
      {
        // n = outer dimension (left & right)
        auto ba_ns = benchmark_axis<index_t>::parse (env_var_default (
            "ns", "[1, 2, 5, 14, 100][300, 1000]"), f_pow (x, 2));

        // p = inner dimension
        auto ba_ps = benchmark_axis<index_t>::parse (env_var_default (
            "ps", "[1, 10, 100][1000]"), f_pow (x, 1));

        auto ba_st = benchmark_axis<supported_type>::parse (
            env_var_default ("types", "[float, double]"),
            f_effort_supported_type ());

        // num_threads
        auto ba_num_threads = benchmark_axis<index_t>::parse (env_var_default (
            "num_threads", "[1, 2, 4]"), f_div (1.0, x));

        auto ba_cpu_target = benchmark_axis<cpu_target>::parse (
            env_var_default ("cpu_targets", "[sse4_2, avx, avx2]"),
            f_effort_cpu_target ());

        return make_benchmark_setup (
            std::move (ba_ns), std::move (ba_ps), std::move (ba_st),
            std::move (ba_num_threads), std::move (ba_cpu_target));
      } ();

      auto candidates = benchmark_axis<identifier>::parse (env_var_default (
          "candidates", "[numa, mkl]"), f_const_t {1.0});

      return benchmark (
          std::move (setup),
          benchmark_map_dot {},
          std::move (candidates),
          f_is_close_supported_type ()) (
              mkl::candidate_dot {},
              candidate_dot { "numa",
                [] (auto const& retval, auto const& A, auto const& B)
                {
                  dot (retval, A, B);
                }
              });
    });
}
