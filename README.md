[![pipeline status](https://gitlab.com/TobyBrull/numa_mkl/badges/master/pipeline.svg)](https://gitlab.com/TobyBrull/numa_mkl/-/commits/master)

# numa\_mkl
Depends on numa and MKL to allow performance comparison between the two.

## For development

```
docker build --rm --build-arg BASE_IMAGE=gcc:10.2 --target dev -t numa_mkl_dev .
docker run --rm -it -v $PWD:/app -w /app numa_mkl_dev bash
```

Then run (some of) the commands from [Dockerfile](Dockerfile) in the
"build" section.

## Building, Testing

See the item 'all.script' in the file [.gitlab-ci.yml](.gitlab-ci.yml).
